#!/usr/bin/env python
import rospy
from camera_pose_calibration.srv import *
from camera_pose_calibration.msg import *

def service_call():
    rospy.wait_for_service('calibrate_topic')
    print "Service Ready"

    pattern_width = 4
    pattern_height = 11
    pattern_distance = 1
    pattern_msg = PatternParameters(pattern_width, pattern_height , pattern_distance, 0,0)

    tag_frame = "tag_frame"
    target_frame = "camera_link"
    point_cloud_scale_x = 1
    point_cloud_scale_y = 1
    pattern = pattern_msg

    try:
        calibrate = rospy.ServiceProxy('calibrate_topic', CalibrateTopic)
        resp1 = calibrate(tag_frame,target_frame, point_cloud_scale_x, point_cloud_scale_y,pattern)
        return resp1.sum
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)


if __name__ == "__main__":

    service_call()
